import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.server.ExportException;
import java.util.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;

public class TroopTool {
	private static int userChoice; //stores the user's last menu choice, used in many methods
	private static String inputFileName;
	private static String outputFileName;
	private static org.json.JSONObject troopData;
	
	public TroopTool() {
		inputFileName = "input.txt";
		outputFileName = "output.txt"; //these are the default names for the files unless the user changes them
	}
	
	public static void main(String[] args) throws IOException, JSONException {
		inputFileName = "input.txt";
		outputFileName = "output.txt";
		System.out.println("Troop Tool by Justin Weagly\r\n" + "----------");
		refreshTroops();
		mainProgram();
	}

	private static void mainProgram() throws IOException, JSONException {
		printMenu();
		userChoice = getChoice(1, 9);
		switch (userChoice) {
		  case 1:
			  selectInput();
			  mainProgram();
		    break;
		  case 2:
			  selectOutput();
			  mainProgram();
		    break;
		  case 3:
			  //refreshTroops();
			  System.out.println(troopData.toString(4));
			  mainProgram();
		    break;
		  case 4:
			  modifyTroop();
			  mainProgram();
		    break;
		  case 5:
			  addTroop();
			  mainProgram();
		    break;
		  case 6:
			  removeTroop();
			  mainProgram();
		    break;
		  case 7:
			  export();
			  mainProgram();
		    break;
		  case 8:
			  aboutApplicant();
			  mainProgram();
			break;
		  case 9:
			System.out.println("Goodbye.");
			System.exit(0);
			break;
		}
	}
	
	

	private static void selectInput() throws IOException, JSONException {
		System.out.println("Enter full file name: ");
		inputFileName = getChoice();
		refreshTroops();
		System.out.println("File name updated to " + inputFileName);
	}

	private static void selectOutput() {
		System.out.println("Enter full file name: ");
		outputFileName = getChoice();
		System.out.println("File name updated to " + outputFileName);
	}

	private static void refreshTroops() throws IOException, JSONException { //this method refreshes the data by pulling from the input file.  
		File tempFile = new File("input.txt");
	    String currentPath = tempFile.getAbsolutePath();
	    //This method is a bit weird with how much extra stuff is going on, but it works! 
		Path path = Paths.get(currentPath);
		String content = Files.readString(path, StandardCharsets.UTF_8);
		troopData = new org.json.JSONObject(content);
		
	}

	private static void modifyTroop() throws JSONException {
		System.out.println("Pick a troop to modify:");
		userChoice = getChoice(1, listTroops());
		int modChoice = userChoice - 1; //saves the chosen troop address for later
		System.out.println("What would you like to modify?");
		printTroopModMenu();
		userChoice = getChoice(1, 6);
		String chosenTrait = "";
		switch (userChoice) {
		  case 1:
			  chosenTrait = "name";
		    break;
		  case 2:
			  chosenTrait = "type";
		    break;
		  case 3:
			  chosenTrait = "damage";
		    break;
		  case 4:
			  chosenTrait = "health";
		    break;
		  case 5:
			  chosenTrait = "target";
		    break;
		  case 6:
			  chosenTrait = "derivedTroops";
		    break;
		}
		System.out.println("Enter what you would like to change " + chosenTrait + " to?");
		if (userChoice == 1 || userChoice == 2 || userChoice == 5) {
			troopData.getJSONArray("troops").getJSONObject(modChoice).put(chosenTrait, getChoice());
		}
		else if (userChoice == 3 || userChoice == 4) {
			troopData.getJSONArray("troops").getJSONObject(modChoice).put(chosenTrait, getChoice(0, 1000));
		}
		else { //this activates if the user chose option 6
			System.out.println("Choose the troop to copy into derived troops.");
			userChoice = getChoice(1, listTroops());
			try {
			troopData.getJSONArray("troops").getJSONObject(modChoice).getJSONArray(chosenTrait).put(troopData.getJSONArray("troops").getJSONObject(userChoice-1));
			}
			catch (Exception e) {
				System.out.println("The chosen troop does not have a derived troops array. Because of this, you are unable to add a derived troop.");
			}
		}
	}
	
	private static void printTroopModMenu() {
		System.out.println("----------\r\n"
				+ "1. name\r\n"
				+ "2. type\r\n"
				+ "3. damage\r\n"
				+ "4. health\r\n"
				+ "5. target\r\n"
				+ "6. derived troops");
	}

	private static void addTroop() throws JSONException {
		System.out.println("New empty troop added to data. Use modifier option to make further modifications");
		troopData.getJSONArray("troops").put(new org.json.JSONObject(" {\r\n"
				+ "    \"name\": \"Default\",\r\n"
				+ "    \"type\": \"Default\",\r\n"
				+ "    \"damage\": 1,\r\n"
				+ "    \"health\": 1,\r\n"
				+ "    \"target\": \"all\",\r\n"
				+ "    \"derivedTroops\": []\r\n"
				+ "  }"));
		
	}

	private static void removeTroop() throws JSONException {
		if (0 < troopData.getJSONArray("troops").length()) {
		System.out.println("Pick a troop to remove:");
		userChoice = getChoice(1, listTroops());
		troopData.getJSONArray("troops").remove(userChoice-1);
		System.out.println("Troop removed");
		}
		else {
		System.out.println("There are no troops left to remove.");
		}
	}

	private static void export() throws JSONException {
		try { //got this code from https://www.w3schools.com/java/java_files_create.asp
		      FileWriter myWriter = new FileWriter(outputFileName);
		      myWriter.write(troopData.toString(4));
		      myWriter.close();
		      System.out.println("Successfully exported to the file.");
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
	}

	private static void aboutApplicant() {
		System.out.println("Thank you for checking out my tools engineer application project! My name is Justin Weagly and I am applying for a position at your company, \r\n"
				+ "Big Huge Games. I had a lot of fun making this project, and would be very interested in talking about it over an interview. There are some \r\n"
				+ "things I would do differently in hindsight, such as use a gui rather than console setup. I would also be happy to attempt the final instruction\r\n"
				+ "of publishing a binary file for the game. The reason I did not complete this final step was because I was a bit confused at the exact instructions, \r\n"
				+ "and would rather look at an example of json to binary conversion to get a better idea of how to go about it. I hope this project convinces you that \r\n"
				+ "I am a good fit for your Tools Engineer position. If it does not, I would still be happy to work any position working with code such as debugging, \r\n"
				+ "developing, testing, or quality assurance. If you have any questions feel free to text me at 443 834 8579.\r\n"
				+ "\r\n"
				+ "Read below for help on using this program:\r\n"
				+ "\r\n"
				+ "If you are having trouble using this program, it is advisable to do the following things:\r\n"
				+ "\r\n"
				+ "-Make sure you have files titled input.txt and output.txt \r\n"
				+ "-fill the input.txt file with your json text \r\n"
				+ "-start the program\r\n"
				+ "-use option 3 to view your troop data \r\n"
				+ "-use options 4, 5, and 6 to make whatever changes you desire\r\n"
				+ "-use option 7 to export to output.txt ");
	}
	
	private static int listTroops() throws JSONException { //this method shows a menu of the troops to pick from and returns how many there are 
		int length = troopData.getJSONArray("troops").length();
		for(int i = 0; i < length; i++) {
			System.out.println(i+1 + ": " + troopData.getJSONArray("troops").getJSONObject(i).getString("name"));
		}
		return length;
	}

	private static void printMenu() {
		System.out.println("----------\r\n"
				+ "Main Menu\r\n"
				+ "==========\r\n"
				+ "1. Select input file\r\n"
				+ "2. Select output file \r\n"
				+ "3. Print troop data\r\n"
				+ "4. Modify a troop\r\n"
				+ "5. Add a new troop \r\n"
				+ "6. Remove a troop \r\n"
				+ "7. Export to output file \r\n"
				+ "8. About the Applicant\r\n"
				+ "9. Close program");
	}

	private static int getChoice(int min, int max) { //this method prompts the user to choose a choice from a menu
		
		Scanner sc = new Scanner(System.in);
		int number;
		do {
		    System.out.println("Enter a number from " + min + " to " + max + "");
		    while (!sc.hasNextInt()) {
		        System.out.println("Invalid input.");
		        sc.next(); // this is important!
		    }
		    number = sc.nextInt();
		} while (number < min || number > max);
		return number;
		//this code was taken from https://stackoverflow.com/questions/3059333/validating-input-using-java-util-scanner
	}
	
	private static String getChoice() {
		Scanner sc = new Scanner(System.in);
		return sc.next();
	}

}
